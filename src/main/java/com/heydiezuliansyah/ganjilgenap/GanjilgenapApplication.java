package com.heydiezuliansyah.ganjilgenap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GanjilgenapApplication {

	public static void main(String[] args) {
		SpringApplication.run(GanjilgenapApplication.class, args);
	}

}
