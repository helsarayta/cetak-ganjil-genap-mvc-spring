package com.heydiezuliansyah.ganjilgenap.models;

import java.util.Arrays;

public class ModelGanjilGenap {
    private int startNumber;
    private int finishNumber;

    public ModelGanjilGenap(int startNumber, int finishNumber) {
        this.startNumber = startNumber;
        this.finishNumber = finishNumber;
    }

    public int getStartNumber() {
        return startNumber;
    }

    public void setStartNumber(int startNumber) {
        this.startNumber = startNumber;
    }

    public int getFinishNumber() {
        return finishNumber;
    }

    public void setFinishNumber(int finishNumber) {
        this.finishNumber = finishNumber;
    }

    @Override
    public String toString() {
        return "ModelGanjilGenap{" +
                "startNumber=" + startNumber +
                ", finishNumber=" + finishNumber +
                '}';
    }

    public String[] printGanjilGenap() {
        String[] strArr = new String[this.finishNumber + 1];

        for (int i = this.startNumber; i <= this.finishNumber; i++) {
            if (i % 2 == 0) {
                strArr[i] = "Angka " + i + " adalah GENAP ";
            } else {

                strArr[i] = "Angka " + i + " adalah GANJIL ";
            }

        }
        return Arrays.stream(strArr).filter(s -> (s != null && s.length() > 0)).toArray(String[]::new);
    }


}
