package com.heydiezuliansyah.ganjilgenap.controller;

import com.heydiezuliansyah.ganjilgenap.models.ModelGanjilGenap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.stereotype.Controller
public class Controller {

    @GetMapping(value = "/")
    public String index() {
        return "form";
    }

    @PostMapping("/")
    public String index
            (
                    @RequestParam String startNumber,
                    @RequestParam String finishNumber,
                    Model model
            ) {
        int firstNumber;
        int secondNumber;

        try {
            firstNumber = Integer.parseInt(startNumber);

        } catch (NumberFormatException e) {
            firstNumber = 0;
        }

        try {

            secondNumber = Integer.parseInt(finishNumber);
        } catch (NumberFormatException e) {
            secondNumber = 0;
        }

        ModelGanjilGenap modelGanjilGenap = new ModelGanjilGenap
                (
                        firstNumber,
                        secondNumber
                );

        String[] result = modelGanjilGenap.printGanjilGenap();
        model.addAttribute("startNumber", firstNumber);
        model.addAttribute("finishNumber", secondNumber);
        model.addAttribute("result", result);


        return "form";
    }

}


